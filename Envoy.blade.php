@servers(['server' => getenv('PROD_SERVER_USER') . '@' . getenv('PROD_SERVER_IP')])

@task('deploy', ['on' => ['server']])
rm -rf {{ getenv('PROD_SERVER_PATH') }}-{{ getenv('CI_COMMIT_TAG') }}

cp -r {{ getenv('PROD_SERVER_PATH') }} {{ getenv('PROD_SERVER_PATH') }}-{{ getenv('CI_COMMIT_TAG') }}

cd {{ getenv('PROD_SERVER_PATH') }}-{{ getenv('CI_COMMIT_TAG') }}

git pull origin {{ getenv('CI_COMMIT_TAG') }}

COMPOSER_MEMORY_LIMIT=-1 composer install --no-interaction --prefer-dist --optimize-autoloader --no-dev

php artisan cache:clear
php artisan route:clear
php artisan config:clear
php artisan config:cache

rm -rf {{ getenv('PROD_SERVER_PATH') }}

mv {{ getenv('PROD_SERVER_PATH') }}-{{ getenv('CI_COMMIT_TAG') }} {{ getenv('PROD_SERVER_PATH') }}
@endtask
